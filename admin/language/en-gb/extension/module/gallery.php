<?php
// Heading
$_['heading_title']       = 'Gallery'; 

// Text
$_['text_module']         = 'Modules';
$_['text_edit_title']         = 'Edit';
$_['text_success']        = 'Success: You have modified module gallery!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_edit']   = 'Edit gallery module';

// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module gallery!';
?>