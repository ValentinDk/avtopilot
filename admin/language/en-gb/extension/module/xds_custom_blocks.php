<?php
// Heading
$_['heading_title']    = '[xds] Custom Blocks';



// Text
$_['text_extension']    = 'Modules';
$_['text_success']     = 'Success: You have modified "Custom Blocks" module!';
$_['text_edit']        = 'Edit';

// Entry
$_['entry_name']       	 = 'Module Name';
$_['entry_status']     	 = 'Status';
$_['entry_image']   		 = 'Image';
$_['entry_description']   = 'Description';
$_['entry_title']        = 'Title';
$_['entry_link']         = 'Link';
$_['entry_sort_order']   = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify "Custom Blocks" module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';

$_['blocks_in_row_text']    = 'Blocks in row';
$_['blocks_text']    = 'Blocks';