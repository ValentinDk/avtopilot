<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Управление парсером';

// Text
$_['text_module']       = 'Модули';
$_['text_extension']    = 'Расширения';
$_['text_success']      = 'Настройки успешно изменены!';
$_['text_add']          = 'Добавление';
$_['text_delete']       = 'Удаление';

// Entry
$_['entry_url']         = 'Ссылка';
$_['entry_urls']        = 'Ссылки';
$_['entry_parser']      = 'Имя парсера';
$_['entry_parent']      = 'Имя родительской категории';
$_['entry_markup']      = 'Наценка, %';
$_['entry_status']      = 'Статус';

// Error
$_['error_permission']  = 'У вас недостаточно прав для внесения изменений!';
$_['error_url']         = 'Некорректная ссылка';
$_['error_parser']      = 'Название должно содержать от 3 до 64 символов!';
$_['error_parent']      = 'Нет такой категории';
$_['error_markup']      = 'Некорректная наценка';