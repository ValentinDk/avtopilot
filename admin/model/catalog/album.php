<?php
class ModelCatalogAlbum extends Model {
	public function addAlbum($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "album SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', thum_width = '" . (int)$data['thum_width'] . "', thum_height = '" . (int)$data['thum_height'] . "', pop_width = '" . (int)$data['pop_width'] . "', pop_height = '" . (int)$data['pop_height'] . "', status = '" . (int)$data['status'] . "'");

		$album_id = $this->db->getLastId();
				
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "album SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE album_id = '" . (int)$album_id . "'");
		}
		
		foreach ($data['album_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "album_description SET album_id = '" . (int)$album_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
		
		if (isset($data['album_images'])) {
			foreach ($data['album_images'] as $album_images) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "album_images SET album_id = '" . (int)$album_id . "', sort_order = '" . (int)$album_images['sort_order'] . "', image = '" . $this->db->escape(html_entity_decode($album_images['image'], ENT_QUOTES, 'UTF-8')) . "'");
				
				$album_images_id = $this->db->getLastId();
				
				foreach ($album_images['album_images_description'] as $language_id => $album_images_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "album_images_description SET album_images_id = '" . (int)$album_images_id . "', language_id = '" . (int)$language_id . "', album_id = '" . (int)$album_id . "', title = '" . $this->db->escape($album_images_description['title']) . "', description = '" . $this->db->escape($album_images_description['description']) . "'");
				}
			}
		}
		
		// MySQL Hierarchical Data Closure Table Pattern
		$level = 0;
		
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "album_path` WHERE album_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");
		
		foreach ($query->rows as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "album_path` SET `album_id` = '" . (int)$album_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");
			
			$level++;
		}
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "album_path` SET `album_id` = '" . (int)$album_id . "', `path_id` = '" . (int)$album_id . "', `level` = '" . (int)$level . "'");

		if (isset($data['album_store'])) {
			foreach ($data['album_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "album_to_store SET album_id = '" . (int)$album_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['album_layout'])) {
			foreach ($data['album_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "album_to_layout SET album_id = '" . (int)$album_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'album_id=" . (int)$album_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
		
		$this->cache->delete('album');
	}
	
	public function editAlbum($album_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "album SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', thum_width = '" . (int)$data['thum_width'] . "', thum_height = '" . (int)$data['thum_height'] . "', pop_width = '" . (int)$data['pop_width'] . "', pop_height = '" . (int)$data['pop_height'] . "', status = '" . (int)$data['status'] . "' WHERE album_id = '" . (int)$album_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "album SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE album_id = '" . (int)$album_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "album_description WHERE album_id = '" . (int)$album_id . "'");

		foreach ($data['album_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "album_description SET album_id = '" . (int)$album_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "album_images WHERE album_id = '" . (int)$album_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "album_images_description WHERE album_id = '" . (int)$album_id . "'");
		
		if (isset($data['album_images'])) {
			foreach ($data['album_images'] as $album_images) {
				if ($album_images['album_images_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "album_images SET album_images_id = '" . (int)$album_images['album_images_id'] . "', sort_order = '" . (int)$album_images['sort_order'] . "', album_id = '" . (int)$album_id . "', image = '" . $this->db->escape(html_entity_decode($album_images['image'], ENT_QUOTES, 'UTF-8')) . "'");
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "album_images SET album_id = '" . (int)$album_id . "', image = '" . $this->db->escape(html_entity_decode($album_images['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$album_images['sort_order'] . "'");
				}
				
				$album_images_id = $this->db->getLastId();
				
				foreach ($album_images['album_images_description'] as $language_id => $album_images_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "album_images_description SET album_images_id = '" . (int)$album_images_id . "', language_id = '" . (int)$language_id . "', album_id = '" . (int)$album_id . "', title = '" . $this->db->escape($album_images_description['title']) . "', description = '" . $this->db->escape($album_images_description['description']) . "'");
				}
			}
		}
		
		// MySQL Hierarchical Data Closure Table Pattern
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "album_path` WHERE path_id = '" . (int)$album_id . "' ORDER BY level ASC");
		
		if ($query->rows) {
			foreach ($query->rows as $album_path) {
				// Delete the path below the current one
				$this->db->query("DELETE FROM `" . DB_PREFIX . "album_path` WHERE album_id = '" . (int)$album_path['album_id'] . "' AND level < '" . (int)$album_path['level'] . "'");
				
				$path = array();
				
				// Get the nodes new parents
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "album_path` WHERE album_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");
				
				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}
				
				// Get whats left of the nodes current path
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "album_path` WHERE album_id = '" . (int)$album_path['album_id'] . "' ORDER BY level ASC");
				
				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}
				
				// Combine the paths with a new level
				$level = 0;
				
				foreach ($path as $path_id) {
					$this->db->query("REPLACE INTO `" . DB_PREFIX . "album_path` SET album_id = '" . (int)$album_path['album_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");
					
					$level++;
				}
			}
		} else {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "album_path` WHERE album_id = '" . (int)$album_id . "'");
			
			// Fix for records with no paths
			$level = 0;
			
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "album_path` WHERE album_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");
			
			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "album_path` SET album_id = '" . (int)$album_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");
				
				$level++;
			}
			
			$this->db->query("REPLACE INTO `" . DB_PREFIX . "album_path` SET album_id = '" . (int)$album_id . "', `path_id` = '" . (int)$album_id . "', level = '" . (int)$level . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "album_to_store WHERE album_id = '" . (int)$album_id . "'");

		if (isset($data['album_store'])) {
			foreach ($data['album_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "album_to_store SET album_id = '" . (int)$album_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "album_to_layout WHERE album_id = '" . (int)$album_id . "'");

		if (isset($data['album_layout'])) {
			foreach ($data['album_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "album_to_layout SET album_id = '" . (int)$album_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}
		/*
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'album_id=" . (int)$album_id. "'");
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'album_id=" . (int)$album_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}*/
		
		$this->cache->delete('album');
	}
	
	public function deleteAlbum($album_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "album_path WHERE album_id = '" . (int)$album_id . "'");
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_path WHERE path_id = '" . (int)$album_id . "'");
			
		foreach ($query->rows as $result) {	
			$this->deleteAlbum($result['album_id']);
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "album WHERE album_id = '" . (int)$album_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "album_description WHERE album_id = '" . (int)$album_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'album_id=" . (int)$album_id . "'");
		
		$this->cache->delete('album');
	} 
	
	// Function to repair any erroneous categories that are not in the album path table.
	public function repairCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album WHERE parent_id = '" . (int)$parent_id . "'");
		
		foreach ($query->rows as $album) {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "album_path` WHERE album_id = '" . (int)$album['album_id'] . "'");
			
			// Fix for records with no paths
			$level = 0;
			
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "album_path` WHERE album_id = '" . (int)$parent_id . "' ORDER BY level ASC");
			
			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "album_path` SET album_id = '" . (int)$album['album_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");
				
				$level++;
			}
			
			$this->db->query("REPLACE INTO `" . DB_PREFIX . "album_path` SET album_id = '" . (int)$album['album_id'] . "', `path_id` = '" . (int)$album['album_id'] . "', level = '" . (int)$level . "'");
						
			$this->repairCategories($album['album_id']);
		}
	}
			
	public function getAlbum($album_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR ' &gt; ') FROM " . DB_PREFIX . "album_path cp LEFT JOIN " . DB_PREFIX . "album_description cd1 ON (cp.path_id = cd1.album_id AND cp.album_id != cp.path_id) WHERE cp.album_id = c.album_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.album_id) AS path FROM " . DB_PREFIX . "album c LEFT JOIN " . DB_PREFIX . "album_description cd2 ON (c.album_id = cd2.album_id) WHERE c.album_id = '" . (int)$album_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row;
	} 
	
	public function getCategories($data) {
		$sql = "SELECT cp.album_id AS album_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR ' &gt; ') AS name, c.parent_id, c.sort_order FROM " . DB_PREFIX . "album_path cp LEFT JOIN " . DB_PREFIX . "album c ON (cp.path_id = c.album_id) LEFT JOIN " . DB_PREFIX . "album_description cd1 ON (c.album_id = cd1.album_id) LEFT JOIN " . DB_PREFIX . "album_description cd2 ON (cp.album_id = cd2.album_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.album_id ORDER BY name";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		 
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
						
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
				
	public function getAlbumDescriptions($album_id) {
		$album_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_description WHERE album_id = '" . (int)$album_id . "'");
		
		foreach ($query->rows as $result) {
			$album_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'     => $result['meta_title'],
				'meta_keyword'     => $result['meta_keyword'],
				'meta_description' => $result['meta_description'],
				'description'      => $result['description']
			);
		}
		
		return $album_description_data;
	}	
	
	public function getAlbumFilters($album_id) {
		$album_filter_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_filter WHERE album_id = '" . (int)$album_id . "'");
		
		foreach ($query->rows as $result) {
			$album_filter_data[] = $result['filter_id'];
		}

		return $album_filter_data;
	}

	
	public function getAlbumStores($album_id) {
		$album_store_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_to_store WHERE album_id = '" . (int)$album_id . "'");

		foreach ($query->rows as $result) {
			$album_store_data[] = $result['store_id'];
		}
		
		return $album_store_data;
	}

	public function getAlbumLayouts($album_id) {
		$album_layout_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_to_layout WHERE album_id = '" . (int)$album_id . "'");
		
		foreach ($query->rows as $result) {
			$album_layout_data[$result['store_id']] = $result['layout_id'];
		}
		
		return $album_layout_data;
	}
		
	public function getTotalCategories() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "album");
		
		return $query->row['total'];
	}	
		
	public function getTotalCategoriesByImageId($image_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "album WHERE image_id = '" . (int)$image_id . "'");
		
		return $query->row['total'];
	}

	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "album_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}		
	
	public function getAlbumValueDescriptions($album_id) {
		$album_images_data = array();
		
		$album_images_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_images WHERE album_id = '" . (int)$album_id . "'");
				
		foreach ($album_images_query->rows as $album_images) {
			$album_images_description_data = array();
			
			$album_images_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_images_description WHERE album_images_id = '" . (int)$album_images['album_images_id'] . "'");			
			
			foreach ($album_images_description_query->rows as $album_images_description) {
				$album_images_description_data[$album_images_description['language_id']] = array('title' => $album_images_description['title'], 'description' => $album_images_description['description']);
			}
			
			$album_images_data[] = array(
				'album_images_id'          => $album_images['album_images_id'],
				'sort_order'          => $album_images['sort_order'],
				'album_images_description' => $album_images_description_data,
				'image'                    => $album_images['image']
			);
		}
		
		return $album_images_data;
	}
}
?>