<?php
class ModelExtensionModuleParser extends Model {
    public function addUrl($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "url_for_parse SET url = '" . $data['url'] . "', `parser_name` = '" . $data['parser'] . "', `parent_id` = '" . (int) $this->getMainCategoryByName($data['parent']) . "', `markup` = '" . (int)$data['markup'] . "', `status` = '" . $data['status'] . "'");
    }

    public function getUrls() {
        $sql = "SELECT * FROM " . DB_PREFIX . "url_for_parse";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function deleteUrlById($urlId) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_for_parse WHERE id = '" . $urlId . "'");
    }

    public function getMainCategoryByName($name) {
        $sqlForCategoryName = "SELECT `category_id` FROM " . DB_PREFIX . "category_description WHERE `name` = '" . $name . "'";
        $queryForCategoryName = $this->db->query($sqlForCategoryName);
        $category = $queryForCategoryName->row;
        $parentId = (int) $category['category_id'];

        $sql = "SELECT `category_id` FROM " . DB_PREFIX . "category WHERE `parent_id` = '" . 0 . "' AND `category_id` = '" . $parentId . "'";
        $query = $this->db->query($sql);

        return (int) $query->row['category_id'];
    }
    
    public function getCategoryNameById($id) {
        $sql = "SELECT `name` FROM " . DB_PREFIX . "category_description WHERE `category_id` = '" . $id ."'";

        $query = $this->db->query($sql);

        return $query->row['name'];
    }
}