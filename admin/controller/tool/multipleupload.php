<?php
class ControllerToolMultipleUpload extends Controller {
	//$folder = null;
	public function index() {
		$this->load->language('tool/upload');

		$json = array();
		if($this->request->server['HTTPS']){
			$server = HTTPS_CATALOG;
		}else{
			$server = HTTP_CATALOG;
		}
		
		$dir_image = DIR_IMAGE . 'catalog/';
		//If directory doesnot exists create it.
		if (isset($this->session->data['folder'])) {
			if(is_dir($dir_image . $this->session->data['folder'])){
			   $output_dir = $dir_image . $this->session->data['folder'] . '/';
			}elseif(mkdir($dir_image . $this->session->data['folder'])){
				$output_dir = $dir_image . $this->session->data['folder'] . '/';
			}
		}else{
			$output_dir = $dir_image;
		}
	
		if(isset($_FILES["myfile"]))
		{
			$ret = array();

			$error =$_FILES["myfile"]["error"];
		   {
			
				if(!is_array($_FILES["myfile"]['name'])) //single file
				{
					//$RandomNum   = time();
					$filename = $_FILES['myfile']['name'];
					//$ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));
					//$ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.
				 
					//$ImageExt = substr($ImageName, strrpos($ImageName, '.'));
					//$ImageExt       = str_replace('.','',$ImageExt);
					//$ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
					//$NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt;
					//$NewImageName = $fileName; //. '.' . md5(mt_rand());
					/*
					$temp = explode('.',$filename);
					$image = $filename;
					if(isset($this->session->data['folder'])){
						$image = $this->session->data['folder'] . '/' . $filename;
					}
					*/
					//temp[0] is product model

					move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $filename);
					
					 //echo "<br> Error: ".$_FILES["myfile"]["error"];
					// Hide the uploaded file name so people can not link to it directly.
					//$this->load->model('tool/upload');
					//$this->model_tool_upload->changeProductImage($temp[0], $image); 
					//$json['code']= $code;
					//$json['newfilename']= $NewImageName;
					
					//$ret[$NewImageName]= $output_dir.$NewImageName;
				}
				else
				{
					$fileCount = count($_FILES["myfile"]['name']);
					for($i=0; $i < $fileCount; $i++)
					{
						//$RandomNum   = time();
						$filename = $_FILES['myfile']['name'][$i];
						
						/*$temp = explode('.',$filename);
						$image = $filename;
						if(isset($this->session->data['folder'])){
							$image = $this->session->data['folder'] . '/' . $filename;
						} */
						//temp[0] is product model
						
						move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $filename);
						
						 //echo "<br> Error: ".$_FILES["myfile"]["error"];
						// Hide the uploaded file name so people can not link to it directly.
						//$this->load->model('tool/upload');
						//$this->model_tool_upload->changeProductImage($temp[0], $image); 
					}
				}
				/*
				$this->load->model('tool/image');
				if (isset($this->session->data['folder']) {
					$img = 'image/catalog/' . isset($this->session->data['folder'] . '/' . $filename;
				} else {
					$img = 'image/catalog/' . $filename;
				}
				
				$json['imgsrc'] = $this->model_tool_image->resize($img, 100, 100);
				*/
				
				$temp = explode("image/", $output_dir);
				$json['server'] = $server;
				$json['output'] = $temp[1];
			}
			//echo json_encode($ret);
			//$this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));
		}else{
			$error = 'asf';
			//echo json_encode($ret);
		}
	}
	
	public function setFolder(){
		$json = array();
		if (isset($this->request->get['folder']) && $this->request->get['folder'] != '') {
			 $this->session->data['folder'] = $this->request->get['folder'];
			 $json['success'] = 'success';
		}else{
			unset($this->session->data['folder']);
			$json['error'] = 'error';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getFolder(){
		$json = array();
		$dir_image = DIR_IMAGE . 'catalog/';
		
		if(!is_dir($dir_image)){
			mkdir($dir_image);
		}
		
		if (isset($this->request->get['folderName'])) {
			$dirs = array_filter(glob(DIR_IMAGE . 'catalog/*'), 'is_dir');
			foreach($dirs as $key => $dir){
				$temp = explode("catalog/", $dir);
				
				$data[] = array(
					'name' => $temp[1],
					'key' => $key
				);	
			}
			if($this->request->get['folderName'] != ''){
				foreach($data as $k => $v){
					 if(stripos($v['name'], $this->request->get['folderName']) !== false){
						$json[] = array(
							'name' => $v['name'],
							'key' => $k
						);	
					 }	
				}
			}else{
				$json = $data;
			}
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function deleteFile(){
		$json = array();
		if (isset($this->request->get['code'])) {
			$this->load->model('tool/upload');
			$this->model_tool_upload->deleteFilebyCode($this->request->get['code']);
			$json['success']='delete success';
		}else{
			$json['error']='not exit the code';
		}
		
		$this->response->setOutput(json_encode($json));
	}
}
?>