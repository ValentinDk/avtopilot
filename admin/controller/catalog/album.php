<?php 
class ControllerCatalogAlbum extends Controller { 
	private $error = array();
 
	public function index() {
		$this->language->load('catalog/album');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/album');
		 
		$this->getList();
	}

	public function add() {
		$this->language->load('catalog/album');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/album');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_album->addAlbum($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			
			$this->response->redirect($this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->language->load('catalog/album');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/album');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_album->editAlbum($this->request->get['album_id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->response->redirect($this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'] . $url, true));			
			//$this->redirect($this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/album');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/album');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $album_id) {
				$this->model_catalog_album->deleteAlbum($album_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->response->redirect($this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}
		
		$this->getList();
	}
	
	public function repair() {
		$this->language->load('catalog/album');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/album');
		
		if ($this->validateRepair()) {
			$this->model_catalog_album->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'], true));
		}
		
		$this->getList();	
	}
	
	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
   		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);
									
		$data['add'] = $this->url->link('catalog/album/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/album/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/album/repair', 'user_token=' . $this->session->data['user_token'] . $url, true);
		
		$data['categories'] = array();
		
		$filter_data = array(
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		
		//print_r($filter_data['start']);
				
		$album_total = $this->model_catalog_album->getTotalCategories();
		
		$results = $this->model_catalog_album->getCategories($filter_data);

		foreach ($results as $result) {
			
			$data['categories'][] = array(
				'album_id' => $result['album_id'],
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
				'selected'    => isset($this->request->post['selected']) && in_array($result['album_id'], $this->request->post['selected']),
				'sort_order'  => $result['sort_order'],
				'edit'        => $this->url->link('catalog/album/edit', 'user_token=' . $this->session->data['user_token'] . '&album_id=' . $result['album_id'] . $url, true),
				'delete'      => $this->url->link('catalog/album/delete', 'user_token=' . $this->session->data['user_token'] . '&album_id=' . $result['album_id'] . $url, true)
			);
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_insert'] = $this->language->get('button_insert');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
 
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $album_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
			
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($album_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($album_total - $this->config->get('config_limit_admin'))) ? $album_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $album_total, ceil($album_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/album_list', $data));		
		//$this->response->setOutput($this->render());
	}

	protected function getForm() {
		//print_r($this->session->data['folder']);
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_parent'] = $this->language->get('entry_parent');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_column'] = $this->language->get('entry_column');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_top'] = $this->language->get('help_top');
		$data['help_column'] = $this->language->get('help_column');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');
		
		$this->document->addStyle('view/javascript/jquery/upload-multiple/uploadcss.css');
		$this->document->addScript('view/javascript/jquery/upload-multiple/js/jquery.fileuploadmulti.min.js');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
	
 		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], true),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'], true),
      		'separator' => ' :: '
   		);
		if (!isset($this->request->get['album_id'])) {
			$data['action'] = $this->url->link('catalog/album/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/album/edit', 'user_token=' . $this->session->data['user_token'] . '&album_id=' . $this->request->get['album_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/album', 'user_token=' . $this->session->data['user_token'] . $url, true);
		
		if (isset($this->request->get['album_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$album_info = $this->model_catalog_album->getAlbum($this->request->get['album_id']);
    	}
		
		$data['user_token'] = $this->session->data['user_token'];
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['album_description'])) {
			$data['album_description'] = $this->request->post['album_description'];
		} elseif (isset($this->request->get['album_id'])) {
			$data['album_description'] = $this->model_catalog_album->getAlbumDescriptions($this->request->get['album_id']);
		} else {
			$data['album_description'] = array();
		}
		
		if (isset($this->request->post['album_images'])) {
			$album_imagess = $this->request->post['album_images'];
		} elseif (isset($this->request->get['album_id'])) {
			$album_imagess = $this->model_catalog_album->getAlbumValueDescriptions($this->request->get['album_id']);
		} else {
			$album_imagess = array();
		}
		
		$this->load->model('tool/image');
		
		$data['album_imagess'] = array();
		 
		foreach ($album_imagess as $album_images) {
			if ($album_images['image'] && file_exists(DIR_IMAGE . $album_images['image'])) {
				$image = $album_images['image'];
			} else {
				$image = 'no_image.jpg';
			}
			
			$data['album_imagess'][] = array(
				'album_images_id'          => $album_images['album_images_id'],
				'sort_order'          => $album_images['sort_order'],
				'album_images_description' => $album_images['album_images_description'],
				'image'                    => $image,
				'thumb'                    => $this->model_tool_image->resize($image, 100, 100)
			);
		}

		$data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		if (isset($this->request->post['path'])) {
			$data['path'] = $this->request->post['path'];
		} elseif (!empty($album_info)) {
			$data['path'] = $album_info['path'];
		} else {
			$data['path'] = '';
		}
		
		if (isset($this->request->post['thum_width'])) {
			$data['thum_width'] = $this->request->post['thum_width'];
		} elseif (!empty($album_info)) {
			$data['thum_width'] = $album_info['thum_width'];
		} else {
			$data['thum_width'] = 150;
		}
		if (isset($this->request->post['thum_height'])) {
			$data['thum_height'] = $this->request->post['thum_height'];
		} elseif (!empty($album_info)) {
			$data['thum_height'] = $album_info['thum_height'];
		} else {
			$data['thum_height'] = 150;
		}
		if (isset($this->request->post['pop_width'])) {
			$data['pop_width'] = $this->request->post['pop_width'];
		} elseif (!empty($album_info)) {
			$data['pop_width'] = $album_info['pop_width'];
		} else {
			$data['pop_width'] = '';
		}
		if (isset($this->request->post['pop_height'])) {
			$data['pop_height'] = $this->request->post['pop_height'];
		} elseif (!empty($album_info)) {
			$data['pop_height'] = $album_info['pop_height'];
		} else {
			$data['pop_height'] = '';
		}
		
		if (isset($this->request->post['parent_id'])) {
			$data['parent_id'] = $this->request->post['parent_id'];
		} elseif (!empty($album_info)) {
			$data['parent_id'] = $album_info['parent_id'];
		} else {
			$data['parent_id'] = 0;
		}
		
		$this->load->model('setting/store');

		$data['stores'] = array();
		
		$data['stores'][] = array(
			'store_id' => 0,
			'name'     => $this->language->get('text_default')
		);
		
		$stores = $this->model_setting_store->getStores();

		foreach ($stores as $store) {
			$data['stores'][] = array(
				'store_id' => $store['store_id'],
				'name'     => $store['name']
			);
		}

		if (isset($this->request->post['album_store'])) {
			$data['album_store'] = $this->request->post['album_store'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['album_store'] = $this->model_catalog_album->getAlbumStores($this->request->get['category_id']);
		} else {
			$data['album_store'] = array(0);
		}
		/*
		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($album_info)) {
			$data['keyword'] = $album_info['keyword'];
		} else {
			$data['keyword'] = '';
		}
		*/
		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($album_info)) {
			$data['image'] = $album_info['image'];
		} else {
			$data['image'] = '';
		}

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($album_info) && is_file(DIR_IMAGE . $album_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($album_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['top'])) {
			$data['top'] = $this->request->post['top'];
		} elseif (!empty($album_info)) {
			$data['top'] = $album_info['top'];
		} else {
			$data['top'] = 0;
		}

		if (isset($this->request->post['column'])) {
			$data['column'] = $this->request->post['column'];
		} elseif (!empty($album_info)) {
			$data['column'] = $album_info['column'];
		} else {
			$data['column'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($album_info)) {
			$data['sort_order'] = $album_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($album_info)) {
			$data['status'] = $album_info['status'];
		} else {
			$data['status'] = 1;
		}
		
		if (isset($this->request->post['album_layout'])) {
			$data['album_layout'] = $this->request->post['album_layout'];
		} elseif (isset($this->request->get['album_id'])) {
			$data['album_layout'] = $this->model_catalog_album->getAlbumLayouts($this->request->get['album_id']);
		} else {
			$data['album_layout'] = array();
		}

		
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/album_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/album')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['album_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
					
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/album')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
 
		if (!$this->error) {
			return true; 
		} else {
			return false;
		}
	}
	
	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/album')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
 
		if (!$this->error) {
			return true; 
		} else {
			return false;
		}
	}
			
	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/album');
			
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			
			$results = $this->model_catalog_album->getCategories($data);
				
			foreach ($results as $result) {
				$json[] = array(
					'album_id' => $result['album_id'], 
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();
	  
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}		
}
?>