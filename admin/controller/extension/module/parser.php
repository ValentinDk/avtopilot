<?php
class ControllerExtensionModuleParser extends Controller {
	private $error = array();

	public function install() {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "url_for_parse` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `url` varchar(255) NOT NULL,
            `parser_name` varchar(255) NOT NULL,
            `parent_id` int(11) NOT NULL,
            `markup` float NOT NULL,
            `status` tinyint(1) NOT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` CHANGE `date_available` `date_available` DATE NOT NULL;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "category` CHANGE `column` `column` INT(3) NOT NULL DEFAULT '1';");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "category` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "category_description` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "category_path` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "category_to_store` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_description` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_image` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_to_store` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_to_category` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "seo_url` ENGINE=Innodb;");
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "category` ADD COLUMN `is_equipment` BOOLEAN NOT NULL DEFAULT '0';");
    }

	public function index() {
		
		$this->load->model('extension/module/parser');
			
		$this->load->language('extension/module/parser');

		$this->document->setTitle($this->language->get('heading_title'));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->request->post['url'] = str_replace('amp;', '', $this->request->post['url']);
            $this->model_extension_module_parser->addUrl($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_add'] = $this->language->get('text_add');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		
		$data['entry_url'] = $this->language->get('entry_url');
		$data['entry_parser'] = $this->language->get('entry_parser');
		$data['entry_markup'] = $this->language->get('entry_markup');
		$data['entry_parent'] = $this->language->get('entry_parent');
        $data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['url'])) {
			$data['error_url'] = $this->error['url'];
		} else {
			$data['error_url'] = '';
		}

        if (isset($this->error['parser'])) {
            $data['error_parser'] = $this->error['parser'];
        } else {
            $data['error_parser'] = '';
        }

        if (isset($this->error['parent'])) {
            $data['error_parent'] = $this->error['parent'];
        } else {
            $data['error_parent'] = '';
        }

        if (isset($this->error['markup'])) {
            $data['error_markup'] = $this->error['markup'];
        } else {
            $data['error_markup'] = '';
        }
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/parser', 'user_token=' . $this->session->data['user_token'], true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/parser', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/parser', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true);
		}
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		
		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		if (isset($this->request->post['urlId'])) {
		    $this->model_extension_module_parser->deleteUrlById($this->request->post['urlId']);
        }

		$data['urls'] = $this->model_extension_module_parser->getUrls();

		foreach ($data['urls'] as $item => $url) {
        	$data['urls'][$item]['parent_id'] = $this->model_extension_module_parser->getCategoryNameById($url['parent_id']);
        }

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/parser', $data));
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/parser')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (filter_var($this->request->post['url'], FILTER_SANITIZE_URL) === false) {
			$this->error['url'] = $this->language->get('error_url');
		}

        if ((utf8_strlen($this->request->post['parser']) < 3) || (utf8_strlen($this->request->post['parser']) > 64)) {
            $this->error['parser'] = $this->language->get('error_parser');
        }

        if (!$this->model_extension_module_parser->getMainCategoryByName($this->request->post['parent'])) {
        	$this->error['parent'] = $this->language->get('error_parent');
        }

        if (!is_numeric($this->request->post['markup'])) {
            $this->error['markup'] = $this->language->get('error_markup');
        }
		
		return !$this->error;
	}
}