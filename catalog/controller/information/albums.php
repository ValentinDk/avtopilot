<?php 
class ControllerInformationAlbums extends Controller {  
	public function index() { 
		$this->language->load('product/category');
		
		$this->load->model('catalog/album');
		
		$this->load->model('tool/image'); 
		
		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}
				
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}	
							
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
							
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
			
		if (isset($this->request->get['album_id'])) {
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
									
			$album_id = '';
			
			$parts = explode('_', (string)$this->request->get['album_id']);
		
			$category_id = (int)array_pop($parts);
		
			foreach ($parts as $path_id) {
				if (!$album_id) {
					$album_id = (int)$path_id;
				} else {
					$album_id .= '_' . (int)$path_id;
				}
									
				$category_info = $this->model_catalog_album->getAlbum($path_id);
				
				if ($category_info) {
	       			$data['breadcrumbs'][] = array(
   	    				'text'      => $category_info['name'],
						'href'      => $this->url->link('information/albums', 'album_id=' . $album_id . $url)
        			);
				}
			}
		} else {
			$category_id = 0;
		}
				
		$category_info = $this->model_catalog_album->getAlbum($category_id);
		//print_r($category_info);
		if ($category_info) {
	  		$this->document->setTitle($category_info['name']);
			$this->document->setDescription($this->config->get('config_store_name'));
			$this->document->setKeywords($category_info['meta_keyword']);
			//$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');
			$this->document->addScript('catalog/view/javascript/lightbox/js/modernizr.custom.js');
			$this->document->addScript('catalog/view/javascript/lightbox/js/lightbox-2.6.min.js');
			$this->document->addStyle('catalog/view/javascript/lightbox/css/lightbox.css');
			
			$data['heading_title'] = $category_info['name'];
			
			$data['text_refine'] = $this->language->get('text_refine');
			
			
			// Set the last category breadcrumb		
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
									
			$data['breadcrumbs'][] = array(
				'text'      => $category_info['name'],
				'href'      => $this->url->link('information/albums', 'album_id=' . $this->request->get['album_id'])
			);
								
			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
			} else {
				$data['thumb'] = '';
			}
									
			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}	
						
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
			$thum_width = $category_info['thum_width'];
			$thum_height = $category_info['thum_height'];
			$pop_width = $category_info['pop_width'];
			$pop_height = $category_info['pop_height'];
			//print_r($thum_width);
			if($thum_width == 0 || $thum_height ==0){
				$thum_width = $this->config->get($this->config->get('config_theme') . '_image_product_width');
				$thum_height = $this->config->get($this->config->get('config_theme') . '_image_product_height');
			}
			
			
			$data['categories'] = array();
			
			$results = $this->model_catalog_album->getAlbums($category_id);
			
			foreach ($results as $result) {
				
				//$product_total = $this->model_catalog_product->getTotalProducts($data);				
				if ($result['image']) {
					$cimage = $this->model_tool_image->resize($result['image'], $thum_width, $thum_height);
				} else {
					$cimage = '';
				}
				$data['categories'][] = array(
					'name'  => $result['name'],
					'image'  => $cimage,
					'href'  => $this->url->link('information/albums', 'album_id=' . $this->request->get['album_id'] . '_' . $result['album_id'] . $url)
				);
			}
			
			//print_r($data['categories']);
			$data['photos'] = array();
			
			$data_filter = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter, 
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);
			
			
			$photo_total = $this->model_catalog_album->getTotalPhotos($category_id); 
			
			$results = $this->model_catalog_album->getPhotos($category_id);
			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $thum_width, $thum_height );
				} else {
					$image = false;
				}
							
				$data['photos'][] = array(
					'album_id'    => $result['album_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'title'        => $result['title'],
					'href'        => ($pop_height ==0)? 'image/'. $result['image'] : $this->model_tool_image->resize($result['image'], $pop_width, $pop_height),
					'description' => $result['description']
				);
			}
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$data['limits'] = array();
	
			$limits = array_unique(array($this->config->get('config_catalog_limit'), 25, 50, 75, 100));
			
			sort($limits);
	
			foreach($limits as $limits){
				$data['limits'][] = array(
					'text'  => $limits,
					'value' => $limits,
					'href'  => $this->url->link('information/albums', 'album_id=' . $this->request->get['album_id'] . $url . '&limit=' . $limits)
				);
			}
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
	
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
					
			$pagination = new Pagination();
			$pagination->total = $photo_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('information/albums', 'album_id=' . $this->request->get['album_id'] . $url . '&page={page}');
		
			$data['pagination'] = $pagination->render();
		
			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;
		
			$data['continue'] = $this->url->link('common/home');
			
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('information/albums', $data));
			
    	} else {
			//get all album
			$results = $this->model_catalog_album->getAlbums();
			if($results){
				$this->document->setTitle('Album Gallery');
				$this->document->setDescription($this->config->get('config_store_name'));
				$this->document->setKeywords($this->config->get('config_store_name'));
				//$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');
				$this->document->addScript('catalog/view/javascript/lightbox/js/modernizr.custom.js');
				$this->document->addScript('catalog/view/javascript/lightbox/js/lightbox-2.6.min.js');
				$this->document->addStyle('catalog/view/javascript/lightbox/css/lightbox.css');
				
				$data['heading_title'] = 'Album Gallery';
				
				foreach ($results as $result) {
				
					//$product_total = $this->model_catalog_product->getTotalProducts($data);				
					if ($result['image']) {
						$cimage = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					} else {
						$cimage = '';
					}
					$data['categories'][] = array(
						'name'  => $result['name'],
						'image'  => $cimage,
						'href'  => $this->url->link('information/albums', 'album_id=' . $result['album_id'])
					);
				}
				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');
				
				$this->response->setOutput($this->load->view('information/albums', $data));
			}else{
				$url = '';
				if (isset($this->request->get['album_id'])) {
					$url .= '&album_id=' . $this->request->get['album_id'];
				}
				
				if (isset($this->request->get['filter'])) {
					$url .= '&filter=' . $this->request->get['filter'];
				}
													
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}	

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
					
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
							
				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}
							
				$data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_error'),
					'href'      => $this->url->link('product/category', $url),
					'separator' => $this->language->get('text_separator')
				);
					
				$this->document->setTitle($this->language->get('text_error'));

				$data['heading_title'] = $this->language->get('text_error');

				$data['text_error'] = $this->language->get('text_error');

				$data['button_continue'] = $this->language->get('button_continue');

				$data['continue'] = $this->url->link('common/home');

				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');
				$this->response->setOutput($this->load->view('error/not_found', $data));
				
			}
		}
  	}
}
?>