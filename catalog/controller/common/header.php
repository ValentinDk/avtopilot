<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
		
		

		$this->load->model('setting/setting');
			
		$ft_settings = array();
		$ft_settings = $this->model_setting_setting->getSetting('theme_frame', $this->config->get('config_store_id'));
		$language_id = $this->config->get('config_language_id');

		if (isset($ft_settings['t1_main_menu_toggle']) && $ft_settings['t1_main_menu_toggle']){
			$data['links_status'] = $ft_settings['t1_main_menu_toggle'];
		} else {
			$data['links_status'] = false;
		}
		
		if (isset($ft_settings['t1_main_menu_item']) && $ft_settings['t1_main_menu_item']){
			$links = $ft_settings['t1_main_menu_item'];
		} else {
			$links = array();
		}

		if (!empty($links)){
			foreach ($links as $key => $value) {
				$sorted_links[$key] = $value['sort'];
			} 
			array_multisort($sorted_links, SORT_ASC, $links);
		}
		





		$this->load->language('extension/module/frametheme/ft_menu');
		
		$this->load->model('setting/setting');
			
		$ft_settings = array();
		$ft_settings = $this->model_setting_setting->getSetting('theme_frame', $this->config->get('config_store_id'));
		$language_id = $this->config->get('config_language_id');
		
		if (isset($ft_settings['t1_category_mask_toggle']) && $ft_settings['t1_category_mask_toggle']){
			$data['mask_status'] = $ft_settings['t1_category_mask_toggle'];
		} else {
			$data['mask_status'] = false;
		}
		
		if (isset($ft_settings['t1_category_third_level_toggle']) && $ft_settings['t1_category_third_level_toggle']){
			$data['third_level_status'] = $ft_settings['t1_category_third_level_toggle'];
		} else {
			$data['third_level_status'] = false;
		}
		
		if (isset($ft_settings['t1_category_third_level_limit']) && $ft_settings['t1_category_third_level_limit']){
			$data['third_level_limit'] = $ft_settings['t1_category_third_level_limit'];
		} else {
			$data['third_level_limit'] = 5;
		}
		
		if (isset($ft_settings['t1_category_no_full_height_submenu']) && $ft_settings['t1_category_no_full_height_submenu']){
			$data['alt_view_submenu'] = $ft_settings['t1_category_no_full_height_submenu'];
		} else {
			$data['alt_view_submenu'] = false;
		}
		
		if (isset($ft_settings['t1_add_cat_links_toggle']) && $ft_settings['t1_add_cat_links_toggle']){
			$data['add_cat_links_status'] = $ft_settings['t1_add_cat_links_toggle'];
		} else {
			$data['add_cat_links_status'] = false;
		}
		
		if (isset($ft_settings['t1_category_icon']) && $ft_settings['t1_category_icon']){
			$category_icons = $ft_settings['t1_category_icon'];
		} else {
			$category_icons = array();
		}

		if (isset($ft_settings['t1_add_cat_links_item']) && !empty($ft_settings['t1_add_cat_links_item'])) {
			$results = $ft_settings['t1_add_cat_links_item'];
		} else {
			$results = array();
		}

		$data['add_category_menu'] = array();

		foreach ($results as $result) {
			
			if (is_file(DIR_IMAGE . $result['image_peace'])) {
				$image_peace = $result['image_peace'];
			} else {
				$image_peace = '';
			}
			
			if (is_file(DIR_IMAGE . $result['image_hover'])) {
				$image_hover = $result['image_hover'];
			} else {
				$image_hover = '';
			}
			
			$data['add_category_menu'][] = array(
				'image_peace' => $this->model_tool_image->resize($image_peace, 24, 24),
				'image_hover' => $this->model_tool_image->resize($image_hover, 24, 24),
				'title'			  => html_entity_decode($result['title'][$language_id], ENT_QUOTES, 'UTF-8'),
				'link'  			=> $result['link'][$language_id],
				'position'		=> $result['position'],
				'html'  			=> html_entity_decode($result['html'], ENT_QUOTES, 'UTF-8'),
				'sort'  			=> $result['sort']
			);	
			
		}

		if (!empty($data['add_category_menu'])){
			foreach ($data['add_category_menu'] as $key => $value) {
				$sort_add_category_menu[$key] = $value['sort'];
			} 
			array_multisort($sort_add_category_menu, SORT_ASC, $data['add_category_menu']);
		}


		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);
		
		
		if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		
		
		if (isset($ft_settings['t1_category_shown_pages']) && !empty($ft_settings['t1_category_shown_pages'])) {
			$shown_menu_routes = $ft_settings['t1_category_shown_pages'];
		} else {
			$shown_menu_routes = array('product/category', 'common/home');
		}

		$data['menu_show'] = false;
		
		if (in_array($route, $shown_menu_routes)) {
			$data['menu_show'] = true;
		}
		
		$parts = array();
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		}



		$data['mh_links'] = array();




		foreach ($categories as $category) {
			
			
			
			
			if ($category['top']) {
				
				if (isset($category_icons[$category['category_id']]['html'])) {
					$html_icon = html_entity_decode($category_icons[$category['category_id']]['html'], ENT_QUOTES, 'UTF-8');
				} else {
					$html_icon = '';
				}
				
				if (isset($category_icons[$category['category_id']]['peace']) && is_file(DIR_IMAGE . $category_icons[$category['category_id']]['peace'])) {
					$icon_peace = $this->model_tool_image->resize($category_icons[$category['category_id']]['peace'], 24, 24);
				} else {
					$icon_peace = '';
				}
				
				if (isset($category_icons[$category['category_id']]['hover']) && is_file(DIR_IMAGE . $category_icons[$category['category_id']]['hover'])) {
					$icon_hover = $this->model_tool_image->resize($category_icons[$category['category_id']]['hover'], 24, 24);
				} else {
					$icon_hover = '';
				}

				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {

					// Level 3
					$children2_data = array();
					
					$children2 = $this->model_catalog_category->getCategories($child['category_id']);
					
					foreach ($children2 as $child2) {
						
						$filter_data3 = array(
							'filter_category_id'  => $child2['category_id'],
							'filter_sub_category' => true
						);
						
						$active = false;
						if (isset($parts[2]) && $parts[2] == $child2['category_id']) {
							$active = true;
						}
						
						$children2_data[] = array(
							'active' => $active,
							'name'  => $child2['name'],
							'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'].'_'. $child2['category_id'])
						);
					}
					
					if (isset($category_icons[$child['category_id']]['html'])) {
						$child_html_icon = html_entity_decode($category_icons[$child['category_id']]['html'], ENT_QUOTES, 'UTF-8');
					} else {
						$child_html_icon = '';
					}
					
					if (isset($category_icons[$child['category_id']]['peace']) && is_file(DIR_IMAGE . $category_icons[$child['category_id']]['peace'])) {
						$child_icon_peace = $this->model_tool_image->resize($category_icons[$child['category_id']]['peace'], 24, 24);
					} else {
						$child_icon_peace = '';
					}
					
					if (isset($category_icons[$child['category_id']]['hover']) && is_file(DIR_IMAGE . $category_icons[$child['category_id']]['hover'])) {
						$child_icon_hover = $this->model_tool_image->resize($category_icons[$child['category_id']]['hover'], 24, 24);
					} else {
						$child_icon_hover = '';
					}

					$filter_data2 = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);
					
					$active = false;
					if (isset($parts[1]) && $parts[1] == $child['category_id']) {
						$active = true;
					}

					$children_data[] = array(
						'category_id' => $child['category_id'],
						'icon_peace' => $child_icon_peace,
						'icon_hover' => $child_icon_hover,
						'html_icon' => $child_html_icon,
						'active' => $active,
						'children2'    => $children2_data,
						'name'  => $child['name'],
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$filter_data = array(
					'filter_category_id'  => $category['category_id'],
					'filter_sub_category' => true
				);
				
				$active = false;
				if (isset($parts[0]) && $parts[0] == $category['category_id']) {
					$active = true;
				}
				

				$data['mh_links'][] = array(
					
					'title'  => $category['name'],
					
					
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id']),
					'sort'         => 1,
				);
			}
		}
	
		
		foreach ($links as $link) {
			// if (html_entity_decode($link['title'][$language_id], ENT_QUOTES, 'UTF-8') == 'Главная') {
			// 	continue;
			// }
			$data['mh_links'][] = array(
				'title'        => html_entity_decode($link['title'][$language_id], ENT_QUOTES, 'UTF-8'),
				'href'         => $link['link'][$language_id],
				'sort'         => $link['sort'],
			);
		}














		$this->load->model('setting/extension');

		$data['analytics'] = array();

		$analytics = $this->model_setting_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['robots'] = $this->document->getRobots();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts('header');
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');
		
		
		$host = isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1')) ? HTTPS_SERVER : HTTP_SERVER;
		if ($this->request->server['REQUEST_URI'] == '/') {
			$data['og_url'] = $this->url->link('common/home');
		} else {
			$data['og_url'] = $host . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
		}
		
		$data['og_image'] = $this->document->getOgImage();
		


		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));
		
		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');
		
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['currency'] = $this->load->controller('common/currency');
		if ($this->config->get('configblog_blog_menu')) {
			$data['blog_menu'] = $this->load->controller('blog/menu');
		} else {
			$data['blog_menu'] = '';
		}
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
		$data['menu'] = $this->load->controller('common/menu');

		


		return $this->load->view('common/header', $data);
	}
}
