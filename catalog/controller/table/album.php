<?php

class ControllerTableAlbum extends Controller
{

public function index()
{
	
	$sql="CREATE TABLE  IF NOT EXISTS `".DB_PREFIX."album` (
	 `album_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	 `image` varchar(500) DEFAULT NULL,
	 `parent_id` int(11),
	 `top` int(11),
	 `column` int(11),
	  `sort_order` int(11),
	  `thum_width` int(11),
	  `thum_height` int(11),
	  `pop_width` int(11),
	  `pop_height` int(11),
	  `status` int(1) DEFAULT 1,
	  PRIMARY KEY (`album_id`)
	) ENGINE=MyISAM 
	  
	";
	if($this->db->query($sql)) {
	echo " table album created<br/>";
	}
	
	$sql="CREATE TABLE  IF NOT EXISTS `".DB_PREFIX."album_path` (
	 `album_id` int NOT NULL,
	 `path_id` int NOT NULL,
	 `level` int(11),
	  PRIMARY KEY (album_id,path_id)
	) ENGINE=MyISAM 
	  
	";
	if($this->db->query($sql)) {
	echo " table album to path created<br/>";
	}
	
	$sql="CREATE TABLE  IF NOT EXISTS `".DB_PREFIX."album_description` (
	 `album_description_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	 `album_id` int(11),
	 `name` varchar(500) DEFAULT NULL,
	 `description` TEXT DEFAULT NULL,
	 `meta_title` varchar(500) DEFAULT NULL,
	 `meta_description` varchar(500) DEFAULT NULL,
	 `meta_keyword` varchar(500) DEFAULT NULL,
	 `language_id` int(11),
	  PRIMARY KEY (`album_description_id`)
	) ENGINE=MyISAM 
	  
	";
	if($this->db->query($sql)) {
	echo " table album description created<br/>";
	}
	
	$sql="CREATE TABLE  IF NOT EXISTS `".DB_PREFIX."album_images` (
	 `album_images_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `album_id` int(11),
	  `image` varchar(500) DEFAULT NULL,
	  `sort_order` int(11),
	  PRIMARY KEY (`album_images_id`)
	) ENGINE=MyISAM 
	  
	";
	if($this->db->query($sql)) {
	echo "table album_images created<br/>";
	}
	
	$sql="CREATE TABLE  IF NOT EXISTS `".DB_PREFIX."album_images_description` (
	  `album_images_description_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `album_images_id` int(11),
	  `album_id` int(11),
	  `title` varchar(500) DEFAULT NULL,
	  `description` varchar(500) DEFAULT NULL,
	  `language_id` int(11),
	  PRIMARY KEY (`album_images_description_id`)
	) ENGINE=MyISAM 
	  
	";
	if($this->db->query($sql)) {
	echo "table album_images_description created<br/>";
	}
	
	$sql="CREATE TABLE  IF NOT EXISTS `".DB_PREFIX."album_to_layout` (
	 `album_id` int NOT NULL,
	 `store_id` int NOT NULL,
	 `layout_id` int NOT NULL,
	  PRIMARY KEY (album_id,store_id)
	) ENGINE=MyISAM 
	  
	";
	if($this->db->query($sql)) {
	echo " album to layout created<br/>";
	}

	$sql="CREATE TABLE  IF NOT EXISTS `".DB_PREFIX."album_to_store` (
	 `album_id` int NOT NULL,
	 `store_id` int NOT NULL,
	  PRIMARY KEY (album_id,store_id)
	) ENGINE=MyISAM 
	  
	";
	if($this->db->query($sql)) {
	echo " album to store created<br/>";
	}
	
	// product to album
	$sql="CREATE TABLE  IF NOT EXISTS `".DB_PREFIX."product_to_album` (
	 `product_id` int NOT NULL,
	 `album_id` int NOT NULL,
	  PRIMARY KEY (product_id,album_id)
	) ENGINE=MyISAM 
	  
	";
	if($this->db->query($sql)) {
	echo " table product to album created<br/>";
	}
}

}