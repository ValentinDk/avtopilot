<?php  
class ControllerModuleGallery extends Controller {
	protected $album_id = 0;
	protected $level = 0;
	protected $path = array();
	public function index($setting) {
		$this->language->load('module/gallery');
		
    	$data['heading_title'] = $this->language->get('heading_title');
		$this->load->model('catalog/album');

		if (isset($this->request->get['album_id'])) {
			$this->path = explode('_', $this->request->get['album_id']);
			$this->album_id = end($this->path);
		}
		
		$data['category'] = $this->getAlbums(0,'',$this->level);
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/gallery.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/gallery.tpl', $data);
		} else {
			return $this->load->view('default/template/module/gallery.tpl', $data);
		}
		
  	}
	
	protected function getAlbums($parent_id, $current_path = '', &$level) {
		//$level = count($this->path);
		$output = '';
		//print_r($this->level);
		if($level==5){
			return $output;
		}
		$album_id = array_shift($this->path);
		$results = $this->model_catalog_album->getAlbums($parent_id);
		
		if ($results && $parent_id==0) { 
			$output .= '<ul class="box-category-parent">';
    	}else if($results){
			$output .= '<ul>';
		}
		
		foreach ($results as $result) {	
			$this->level +=1;
			if (!$current_path) {
				$new_path = $result['album_id'];
			} else {
				$new_path = $current_path . '_' . $result['album_id'];
			}
			
			if ($this->album_id == $result['album_id']) {
				$output .= '<li class="active">';
			} else {
				$output .= '<li>';
			}
			$children = '';
			
			if ($album_id == $result['album_id']) {
				$children = $this->getAlbums($result['album_id'], $new_path,$this->level);
			}
			//print_r('parent:' . $parent_id);
			//print_r('curent parent:' . $parent_id);
			if ($this->album_id == $result['album_id']) {
				$output .= '<a class="active" href="' . $this->url->link('information/albums', 'album_id=' . $new_path)  . '">' . $result['name'] . '</a>';
			} else {
				$output .= '<a href="' . $this->url->link('information/albums', 'album_id=' . $new_path)  . '">' . $result['name'] . '</a>';
			}
			
        	$output .= $children;
        	$output .= '</li>'; 
			
		}
 
		if ($results) {
			$output .= '</ul>';
		}
		
		
		return $output;
	}	
}
?>