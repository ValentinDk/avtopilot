<?php
class ModelCatalogAlbum extends Model {
	public function getAlbum($album_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "album c LEFT JOIN " . DB_PREFIX . "album_description cd ON (c.album_id = cd.album_id) WHERE c.album_id = '" . (int)$album_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c.status = '1'");
		return $query->row;
	}
	
	public function getAlbums($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album c LEFT JOIN " . DB_PREFIX . "album_description cd ON (c.album_id = cd.album_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}
	
	public function getTotalCategoriesByAlbumId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "album c WHERE c.parent_id = '" . (int)$parent_id . "' AND c.status = '1'");
		
		return $query->row['total'];
	}
	
	public function getPhotos($album_id) {
		$album_images_data = array();
		
		$album_images_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_images ov LEFT JOIN " . DB_PREFIX . "album_images_description ovd ON (ov.album_images_id = ovd.album_images_id) WHERE ov.album_id = '" . (int)$album_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order, LCASE(ovd.title)");
		
		
		foreach ($album_images_query->rows as $album_images) {
			//$name = $this->db->query("SELECT name FROM " . DB_PREFIX . "album_description WHERE album_id='" . (int)$album_images['album_id'] . "' AND language_id = '" . (int)$this->config->get('config_language_id'). "'");
			$album_images_data[] = array(
			    'album_id' => $album_images['album_id'],
				'album_images_id' => $album_images['album_images_id'],
				'title'            => $album_images['title'],
				'name'            =>  '',
				'description'      => $album_images['description'],
				'image'           => $album_images['image']
			);
		}
		
		return $album_images_data;
	}
	
	public function getTotalPhotos($album_id) {
		//$album_images_data = array();
		
		$query = $this->db->query("SELECT count(*) as total FROM " . DB_PREFIX . "album_images ov LEFT JOIN " . DB_PREFIX . "album_images_description ovd ON (ov.album_images_id = ovd.album_images_id) WHERE ov.album_id = '" . (int)$album_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
				
		return $query->row['total'];
	}
	
	public function getAlbumByProductID($product_id) {
		//$album_images_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_album WHERE product_id = '" . (int)$product_id . "'");
				
		return $query->rows;
	}
	
	public function getAlbumsLayoutId($album_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "album_to_layout WHERE album_id = '" . (int)$album_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}
	
}
?>